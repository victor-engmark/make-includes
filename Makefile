CP = cp
DIFF = diff
GREP = grep
MAKE = make

TEMPORARY_DIRECTORY := $(shell mktemp -d)

test_directory = test
xml_test_template = $(test_directory)/test.xml.orig
xml_test_file = $(test_directory)/test.xml
expected_xml_result_file = $(test_directory)/result.xml.test

.PHONY: test
test: test-variables test-xml
	$(MAKE) clean

.PHONY: test-variables
test-variables:
	$(MAKE) variables | $(GREP) -q CURDIR
	$(MAKE) FOO=bar variables | $(GREP) --fixed-strings --quiet 'FOO = bar'
	$(MAKE) FOO=bar variable-FOO | $(GREP) --fixed-strings --quiet 'FOO = bar'

.PHONY: test-xml
test-xml:
	$(CP) $(xml_test_template) $(xml_test_file)
	$(MAKE) METHOD=find sort-xml-files
	$(DIFF) $(xml_test_file) $(expected_xml_result_file)

.PHONY: clean
clean:
	-$(RM) $(xml_test_file)

include *.mk
