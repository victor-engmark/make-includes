# [make-includes](https://gitlab.com/engmark/make-includes/)

[![pipeline status](https://gitlab.com/engmark/make-includes/badges/master/pipeline.svg)](https://gitlab.com/engmark/make-includes/-/commits/master)

Files used by Makefiles in several other projects.

Test
----

    make test
